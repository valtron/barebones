Do NOT edit this theme.

It's meant to be a completely minimal theme.

It also disables wp_autop.

How to use
----------

Copy the "sample-child-theme" folder. Rename it to what you want your theme to be named.
Also change the "Theme Name" at the top of "style.css".

Guidelines
----------

- Rather than putting css in "style.css", put it in the "css" folder
and @import it in "style.css"
- Use the [template hierarchy](http://codex.wordpress.org/images/1/18/Template_Hierarchy.png)
to your advantage
