<?
	// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die('Please do not load this page directly. Thanks!');
	
	if (post_password_required()) {
		?>
			<p>This post is password protected. Enter the password to view comments.</p>
		<?
		return;
	}
?>

<? if (have_comments()) { ?>
	<h3>
		<? get_num_comments($post->ID); ?> to &#8220;<? the_title(); ?>&#8221;
	</h3>
	
	<div>
		<div><? previous_comments_link(); ?></div>
		<div><? next_comments_link(); ?></div>
	</div>
	
	<ol>
		<? wp_list_comments(); ?>
	</ol>
	
	<div>
		<div><? previous_comments_link() ?></div>
		<div><? next_comments_link() ?></div>
	</div>
<? } else if (!comments_open()) { ?>
	<p>Comments are closed.</p>
<? } ?>

<? if (comments_open()) { ?>
	<div>
		<h3><? comment_form_title('Leave a Reply', 'Leave a Reply to %s'); ?></h3>
		
		<div>
			<small><? cancel_comment_reply_link(); ?></small>
		</div>
		
		<? if (get_option('comment_registration') and !is_user_logged_in()) { ?>
			<p>You must be <a href="<?= wp_login_url(get_permalink()) ?>">logged in</a> to post a comment.</p>
		<? } else { ?>
			<form action="<?= get_option('siteurl') ?>/wp-comments-post.php" method="post">
				<? if (is_user_logged_in()) { ?>
					<p>
						Logged in as <a href="<?= get_option('siteurl'); ?>/wp-admin/profile.php"><?= $user_identity ?></a>.
						<a href="<?= wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a>
					</p>
				<? } else { ?>
					<p>
						<input type="text" name="author" id="author" value="<?= esc_attr($comment_author) ?>" size="22" tabindex="1" <? if ($req) echo "aria-required='true'"; ?> />
						<label for="author">Name <? if ($req) echo "(required)"; ?></label>
					</p>
					
					<p>
						<input type="text" name="email" id="email" value="<?= esc_attr($comment_author_email) ?>" size="22" tabindex="2" <? if ($req) echo "aria-required='true'"; ?> />
						<label for="email">E-Mail - will not be published <? if ($req) echo "(required)"; ?></label>
					</p>
					
					<p>
						<input type="text" name="url" id="url" value="<?= esc_attr($comment_author_url) ?>" size="22" tabindex="3" />
						<label for="url">Website</label>
					</p>
				<? } ?>
				
				<p><textarea name="comment" id="comment" cols="60%" rows="10" tabindex="4"></textarea></p>
				
				<p>
					<button type="submit" name="submit" tabindex="5">Submit</button>
					<? comment_id_fields(); ?>
				</p>
				
				<? do_action('comment_form', $post->ID); ?>
				
				<input type="hidden" name="comment_post_ID" value="<?= $post->ID; ?>" />
			</form>
		<? } ?>
	</div>
<? } ?>