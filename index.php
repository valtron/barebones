<? get_header(); ?>
	<? if (!have_posts()) { ?>
		<p>Coming Soon</p>
	<? } ?>
	
	<? while (have_posts()) {
		the_post();
	?>
		<div>
			<div><? the_time('F j, Y'); ?></div>
			
			<h3>
				<a href="<? the_permalink(); ?>"><? the_title(); ?></a>
			</h3>
			
			<div>
				By: <span><? the_author(); ?></span>
			</div>
			
			<div>
				<? if (is_single()) { ?>
					<? the_content(); ?>
				<? } else { ?>
					<? the_excerpt(); ?>
				<? } ?>
			</div>
		</div>
	<? } ?>
<? get_footer(); ?>