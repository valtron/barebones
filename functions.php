<?

add_theme_support('post-thumbnails');
add_theme_support('custom-header');
add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
add_theme_support('custom-logo');
add_theme_support('title-tag');

// ACF likes to remove functionality
// https://wpbeaches.com/wordpress-custom-fields-missing-acf-active
add_filter('acf/settings/remove_wp_meta_box', '__return_false');
